﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PROTECTED/StudentLayer/MasterStudent.master" AutoEventWireup="true" CodeFile="DaneStudent.aspx.cs" Inherits="PROTECTED_StudentLayer_DaneStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <center><h1>DANE STUDENTA</h1></center>
        <asp:button runat="server" text="Powrót do poprzedniej strony" onclick="Powrot_Click" />
        <br />
        <br />
        <h3> Możesz edytować swoje dane w tabeli poprzez kliknięcie przycisku "Edytuj" a następnie "Aktualizuj".</h3>
         
        
    </div>

    <div>
        <asp:GridView ID="GridView_DaneStudent" runat="server" Visible="False" AutoGenerateColumns="False" EnableModelValidation="True" DataKeyNames="Id" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
            <Columns>
                 
                    <asp:CommandField ShowEditButton="True" HeaderText="Edytuj dane" ButtonType="Button" />
                    <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
                    <asp:BoundField DataField="nr_indeksu" HeaderText="Numer indeksu" ReadOnly="True" />
                    <asp:BoundField DataField="imie" HeaderText="Imię" />
                    <asp:BoundField DataField="nazwisko" HeaderText="Nazwisko" />
                    <asp:BoundField DataField="adres" HeaderText="Adres" />
                    <asp:BoundField DataField="data_urodzenia" HeaderText="Data urodzenia" />
                    <asp:BoundField DataField="pesel" HeaderText="Pesel" ReadOnly="True" />
                    <asp:BoundField DataField="data_rozpoczecia" HeaderText="Data rozpoczęcia studiów" ReadOnly="True"  />
                    <asp:BoundField DataField="aktualny_sem" HeaderText="Aktualny semestr" ReadOnly="True"  />
                    <asp:BoundField DataField="tryb_studiow" HeaderText="Tryb studiów" ReadOnly="True"  />
                    <asp:BoundField DataField="idkierunku" HeaderText="Kierunek" ReadOnly="True"  />
                    <asp:BoundField DataField="mail" HeaderText="Adres e-mail" ReadOnly="True" />
                    <asp:BoundField DataField="haslo" HeaderText="Hasło" />
                </Columns>
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle BackColor="White" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            </asp:GridView>
        </div>
</asp:Content>

