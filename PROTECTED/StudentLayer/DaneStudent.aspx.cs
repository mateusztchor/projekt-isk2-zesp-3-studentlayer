﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class PROTECTED_StudentLayer_DaneStudent : System.Web.UI.Page
{
    private static string path = HttpContext.Current.Server.MapPath("~/App_Data/DziekanatDB.mdf");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DaneStudent();
        }

    }
    protected void DaneStudent()
    {
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        command.CommandText =
        String.Format("select Id, nr_indeksu, imie, nazwisko, adres, data_urodzenia, pesel, data_rozpoczecia, aktualny_sem, tryb_studiow, Idkierunku, mail, haslo from Studenci where Studenci.nr_indeksu = '{0}' and Studenci.haslo = '{1}'",
        (string)Session["studentLogin"], (string)Session["studentHaslo"]);
        command.CommandType = CommandType.Text;
        SqlDataAdapter adapter = new SqlDataAdapter(command);
        DataTable dt = new DataTable();
        adapter.Fill(dt);
        GridView_DaneStudent.DataSource = dt;
        GridView_DaneStudent.DataBind();
        GridView_DaneStudent.Visible = true;
        connection.Close();
    }

    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        String id = GridView_DaneStudent.DataKeys[e.RowIndex]["Id"].ToString();
        String connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + path + ";Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
        SqlConnection connection = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();
        command.Connection = connection;
        for (int i = 0; i < GridView_DaneStudent.Columns.Count; i++)
        {
            DataControlFieldCell objCell = (DataControlFieldCell)GridView_DaneStudent.Rows[e.RowIndex].Cells[i];
            GridView_DaneStudent.Columns[i].ExtractValuesFromCell(e.NewValues, objCell, DataControlRowState.Edit, false);
        }
        try
        {
            if (String.IsNullOrEmpty(e.NewValues["imie"].ToString())||e.NewValues["nazwisko"].ToString().Length==0|| e.NewValues["adres"].ToString().Length == 0 || e.NewValues["data_urodzenia"].ToString().Length == 0 || e.NewValues["haslo"].ToString().Length == 0)
            {
                
                throw new Exception();

            }
            else
            {
                command.CommandType = CommandType.Text;
                command.CommandText = String.Format("update Studenci set imie='{1}', nazwisko='{2}', adres='{3}', data_urodzenia='{4}', haslo='{5}' where Id='{0}'",
                id, e.NewValues["imie"], e.NewValues["nazwisko"], e.NewValues["adres"], e.NewValues["data_urodzenia"], e.NewValues["haslo"]);
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                GridView_DaneStudent.EditIndex = -1;
                DaneStudent();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + "Pomyślnie zaktualizowano" + "');", true);
            }
            
        }
        catch (Exception ex)
        {

            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + "Wystąpił błąd" + "');", true);

        }
        
        
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView_DaneStudent.EditIndex = e.NewEditIndex;
        DaneStudent();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView_DaneStudent.EditIndex = -1;
        DaneStudent();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_DaneStudent.PageIndex = e.NewPageIndex;
        DaneStudent();
    }
    protected void Powrot_Click(object sender, EventArgs e)
    {
        Server.Transfer("StronaStudent.aspx");
    }
}