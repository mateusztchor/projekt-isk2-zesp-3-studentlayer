﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_WykladowcaLayer_MasterWykladowca : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Logiczna.Wykladowca)
        {
            Response.Redirect("~/Logowanie.aspx");
        }
    }
    protected void Wyloguj_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Session.Abandon();
        Session.RemoveAll();
        Response.Redirect("~/Logowanie.aspx"); 
    }
}
